package by.delpavel.ftpMaster.start;


import by.delpavel.ftpMaster.impls.find.Backup;
import by.delpavel.ftpMaster.impls.find.SQLLog;
import by.delpavel.ftpMaster.impls.run.Remover;
import by.delpavel.ftpMaster.interfaces.Find;
import by.delpavel.ftpMaster.interfaces.Run;

import java.util.Map;

public class Start {


    public static void main(String[] args) throws InterruptedException {


        Config config = new Config();
        config.create();
        Launcher launcher;
        Run run;
        Find find;
        StringBuilder codes = new StringBuilder();

        run = new Remover();
        run.run();

        for (Map.Entry properties : Config.properties.entrySet()) {
            if (properties.getKey().toString().contains("kod")) {
                codes.append(properties.getValue().toString()).append(" ");
                launcher = new Launcher(properties.getValue().toString());
                launcher.start();
                Thread.sleep(40000);
            }
        }

        find = new Backup(codes.toString());
        find.find();
        find = new SQLLog(codes.toString());
        find.find();

    }

}
