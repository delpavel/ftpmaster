package by.delpavel.ftpMaster.start;


import by.delpavel.ftpMaster.impls.find.*;
import by.delpavel.ftpMaster.impls.get.*;
import by.delpavel.ftpMaster.impls.rename.*;
import by.delpavel.ftpMaster.impls.run.*;
import by.delpavel.ftpMaster.impls.send.*;
import by.delpavel.ftpMaster.interfaces.*;

class Launcher extends Thread {


    private String kod;

    Launcher(String kod) {
        this.kod = kod;
    }

    @Override
    public void run() {

        Run run;
        Rename rename;
        Take take;
        Submit submit;
        Find find;

        rename = new SysLogRename(kod);
        rename.rename();
        take = new FileExchange(kod);
        take.get();
        take = new CPRecipient(kod);
        take.get();
        take = new TTN(kod);
        take.get();
        rename = new TTNDBF(kod);
        rename.rename();
        take = new OrderRecipient(kod);
        take.get();
        submit = new OrderSender(kod);
        submit.send();
        take = new OpticsRecipient(kod);
        take.get();
        submit = new OpticsSender(kod);
        submit.send();
        submit = new Check(kod);
        submit.send();
        run = new Autoorder(kod);
        run.run();
        run = new Autoexchange(kod);
        run.run();
        submit = new PCSender(kod);
        submit.send();
        find = new Troubleshooting(kod);
        find.find();
        find = new Computer(kod);
        find.find();
        submit = new TxtSender(kod);
        submit.send();

    }
}
