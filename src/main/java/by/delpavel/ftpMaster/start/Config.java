package by.delpavel.ftpMaster.start;


import org.apache.log4j.Logger;

import java.io.*;

import java.util.Properties;


public class Config {

    private final Logger LOG = Logger.getLogger(Config.class);
    public static Properties properties = new Properties();
    private BufferedReader bufferedReader;
    private final String version = "1507031116";


    void create() {

        String myJarPath = Config.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String dirPath = new File(myJarPath).getParent();


        File file = new File(dirPath + "\\Option.txt");
        File fileVersion = new File(dirPath + "\\ftpMasterJar-" + version);
        File ftpFolder = new File(dirPath);

        if (!fileVersion.exists()) {

            LOG.debug("�������� ������ ������ ���������!");
            File[] filesList = ftpFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().contains("ftpMasterJar-");
                }
            });

            if (filesList != null) {
                LOG.debug("������� ������ ������ � ���������� - " + filesList.length);
                for (File ftpMasterJar : filesList) {
                    LOG.debug("�������� ������ ��������� - " + ftpMasterJar.getPath());
                    ftpMasterJar.delete();
                }
            }

            try {
                LOG.debug("������� ����� ������ ��������� - " + fileVersion.getPath());
                fileVersion.createNewFile();
            } catch (IOException e) {
                LOG.error("�� ���� ������� ���� - " + fileVersion + " " + e.getMessage());
            }
        }

        try {
            bufferedReader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            LOG.error("�� ������ ���� - " + file + " " + e.getMessage());
        }
        String getStr;
        try {

            while ((getStr = bufferedReader.readLine()) != null) {
                properties.put(getStr.substring(0, getStr.indexOf(":")), getStr.substring(getStr.indexOf(":") + 1));
                if (getStr.contains("kod")) {
                    properties.put("obmen" + getStr.substring(getStr.indexOf(".") + 1), "0");
                    properties.put("ttn" + getStr.substring(getStr.indexOf(".") + 1), "0");
                    LOG.debug("obmen = " + properties.getProperty("obmen" + getStr.substring(getStr.indexOf(".") + 1)));
                    LOG.debug("ttn = " + properties.getProperty("ttn" + getStr.substring(getStr.indexOf(".") + 1)));
                }
            }
        } catch (IOException e) {
            LOG.error("������ ��� ������ ����� option.txt");
        }
    }
}
