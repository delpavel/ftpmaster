package by.delpavel.ftpMaster.impls.get;

import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Take;
import org.apache.log4j.Logger;


public class OrderRecipient implements Take {


    private final Logger LOG = Logger.getLogger(OrderRecipient.class);
    private String kod;


    public OrderRecipient(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void get() {

        LOG.debug("��������� ������ ������� �� ���� - " + Config.properties.getProperty("kod" + kod));
        String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\Orders\\", "orders").getFiles(Config.properties.getProperty("kod" + kod));
        if (result.contains("������")) {
            LOG.error(result);
        }


    }
}
