package by.delpavel.ftpMaster.impls.get;

import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Take;
import org.apache.log4j.Logger;

import java.io.File;


public class FileExchange implements Take {


    private final Logger LOG = Logger.getLogger(FileExchange.class);
    private String kod;


    public FileExchange(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void get() {

        LOG.debug("������� ��������� ����� ��� ������� ���������� - " + kod);
        String result = new FtpService(Config.properties.getProperty("ftpFolder") + "\\", "obmen").getFiles(kod);
        if (result.contains("������")) {
            LOG.error(result);
            return;
        }

        if (!result.contains("null")) {
            if (new File(Config.properties.getProperty("ftpFolder") + "\\" + kod).delete()) {
                Config.properties.setProperty("obmen" + kod, "1");
                LOG.debug("������ ���� - " + kod);
            } else {
                LOG.debug("��� ����� ��� ������� ���������� - " + kod);
            }
        }

    }
}
