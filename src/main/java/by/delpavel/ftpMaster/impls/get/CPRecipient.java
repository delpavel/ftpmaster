package by.delpavel.ftpMaster.impls.get;


import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Take;
import org.apache.log4j.Logger;

public class CPRecipient implements Take {

    private final Logger LOG = Logger.getLogger(CPRecipient.class);
    private String kod;


    public CPRecipient(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void get() {

        LOG.debug("������� ��������� ����� - " + Config.properties.getProperty("fileCP" + kod));
        String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\CP\\", "for_apteka").getFiles(Config.properties.getProperty("fileCP" + kod));
        if (result.contains("������")) {
            LOG.error(result);
        }
    }
}
