package by.delpavel.ftpMaster.impls.get;

import by.delpavel.ftpMaster.interfaces.Take;
import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.File;


public class OpticsRecipient implements Take {


    private final Logger LOG = Logger.getLogger(OpticsRecipient.class);
    private String kod;


    public OpticsRecipient(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void get() {

        File directory = new File(Config.properties.getProperty("catalogBD" + kod) + "\\optika_in\\");
        if (directory.exists()) {
            LOG.debug("������� ��������� ������ ������ �� ���� - " + Config.properties.getProperty("kod" + kod));
            String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\optika_in\\", "obmen_optika").getFiles(Config.properties.getProperty("kod" + kod));
            if (result.contains("������")) {
                LOG.error(result);
            }
        }


    }
}
