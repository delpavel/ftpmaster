package by.delpavel.ftpMaster.impls.find;


import by.delpavel.ftpMaster.interfaces.Find;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


public class Computer implements Find {

    private final Logger LOG = Logger.getLogger(Computer.class);
    private String kod;


    public Computer(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void find() {

        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));

        Map<String, String> computersLinks = new HashMap();
        Map<String, String> computers = new HashMap();

        int currentHour = Integer.parseInt(formatHour.format(new Date()));

        if (currentHour >= 9 && currentHour <= 22) {


            File fileLinks = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\links.tmp");
            File fileComputers = new File(Config.properties.getProperty("ftpFolder") + "\\computers");

            if (!fileComputers.exists()) {
                try {
                    fileComputers.createNewFile();
                } catch (IOException e) {
                    LOG.error("������ �������� ����� - " + fileComputers.getPath());
                }
            }
            LOG.debug("����� ����������� � - " + fileLinks.getPath());

            if (fileLinks.exists()) {

                BufferedReader linksReader;
                BufferedReader computersReader;
                try {
                    linksReader = new BufferedReader(new FileReader(fileLinks));
                    computersReader = new BufferedReader(new FileReader(fileComputers));
                    LOG.debug("������ ���� - " + fileLinks.getName());
                    LOG.debug("������ ���� - " + fileComputers.getName());
                } catch (FileNotFoundException e) {
                    LOG.error("�� ������ ���� - " + e.getMessage());
                    return;
                }

                String inputComputers;

                try {
                    while ((inputComputers = computersReader.readLine()) != null) {

                        computers.put(inputComputers.substring(0,inputComputers.indexOf(";")), inputComputers.substring(inputComputers.indexOf(";") + 1));
                    }


                } catch (IOException e) {
                    LOG.error("������ ��� ������ ����� computers - " + e.getMessage());
                }

                String inputLinks;
                String timeConnect = "";
                String nameComputer = "";
                try {
                    while ((inputLinks = linksReader.readLine()) != null) {

                        if (inputLinks.contains("Date&Time")) {
                            LOG.debug("�������� ������ �� �������� - " + inputLinks);
                            timeConnect = inputLinks.substring(inputLinks.lastIndexOf("Date&Time\",\"") + 12, inputLinks.lastIndexOf("\"},"));
                            LOG.debug("�������� ���� � ����� - " + timeConnect);
                        }

                        if (inputLinks.contains("ComputerName")) {
                            LOG.debug("�������� ������ � ������ ���������� - " + inputLinks);
                            nameComputer = inputLinks.substring(inputLinks.lastIndexOf("ComputerName\",\"") + 15, inputLinks.lastIndexOf("\"}"));
                            LOG.debug("�������� ��� ��������� - " + nameComputer);
                        }

                        if (!timeConnect.equals("") && !nameComputer.equals("")) {
                            if (!computersLinks.containsKey(nameComputer)) {
                                computersLinks.put(nameComputer, timeConnect);
                            }
                        }
                    }
                    linksReader.close();

                } catch (IOException e) {
                    LOG.error("������ ��� ������ links.tmp - " + e.getMessage());
                }

                for (Map.Entry<String, String> computer : computersLinks.entrySet()) {
                    computers.put(computer.getKey(), computer.getValue());
                }

                FileWriter computersWriter = null;
                try {
                    computersWriter = new FileWriter(fileComputers);
                } catch (IOException e) {
                    LOG.error("�� ����� - " + fileComputers.getPath());
                }

                if (fileComputers.exists()) {
                    for (Map.Entry<String, String> computer : computers.entrySet()) {
                        try {
                            computersWriter.append(computer.getKey() + ";" + computer.getValue() + "\n");
                        } catch (IOException e) {
                            LOG.error("������ ������ � ���� - " + fileComputers.getPath());
                        }
                    }
                    try {
                        computersWriter.flush();
                        computersWriter.close();
                    } catch (IOException e) {
                        LOG.error("������ �������� ����� - " + fileComputers.getPath());
                    }
                }


            }
        }
    }
}
