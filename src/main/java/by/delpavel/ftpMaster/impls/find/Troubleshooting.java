package by.delpavel.ftpMaster.impls.find;


import by.delpavel.ftpMaster.service.EmailService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Find;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Troubleshooting implements Find {

    private final Logger LOG = Logger.getLogger(Troubleshooting.class);
    private String kod;


    public Troubleshooting(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void find() {

        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        SimpleDateFormat formatMinute = new SimpleDateFormat("mm");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));


        String currentDate = formatDate.format(new Date());
        int currentHour = Integer.parseInt(formatHour.format(new Date()));
        int currentMinute = Integer.parseInt(formatMinute.format(new Date()));

        if (currentHour == 6 && currentMinute >= 42) {

            File file = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\1cv7.mlg");
            LOG.debug("����� ������ � ���� - " + file.getPath());

            BufferedReader syslogReader;
            try {
                syslogReader = new BufferedReader(new FileReader(file));
                LOG.debug("������ ���� - " + file.getName());
            } catch (FileNotFoundException e) {
                LOG.error("�� ������ ���� - " + e.getMessage());
                return;
            }

            LOG.debug("����������� ������� ���� - " + currentDate);
            String input;
            // String errorMessage = "";
            //int countError = 0;
            int countGoodDownloads = 0;
            try {
                while ((input = syslogReader.readLine()) != null) {
                    if (input.contains(currentDate)) {
                        if (input.contains("Distr;DistUplSuc;1;;;")) {
                            countGoodDownloads++;
                        }
                        /*if (input.contains("DistUplErr")) {
                            LOG.debug("�������� ������ � ������� - " + input);
                            errorMessage = input.substring(input.lastIndexOf("DistUplErr;5;") + 13, input.lastIndexOf(";;"));
                            LOG.debug("�������� ������ - " + errorMessage);

                            if (!errorMessage.contains("������ �� ���������� ����� �������� ������ ��� ����������� � ������� �������������� ����.") &&
                                    !errorMessage.contains("������ �������� ������������") &&
                                    !errorMessage.equals("")) {
                                countError++;
                                LOG.debug(errorMessage + " � ���������� - " + countError);
                            }
                        }*/
                    }
                }

                LOG.debug("�������� ��������� ������ ������� ��������� � ���������� - " + countGoodDownloads);
                if (countGoodDownloads == 0) {
                    LOG.debug("�������� ������ �� �����!");
                    EmailService email = new EmailService("helpdesk@interfarmax.by", "pbobrov@interfarmax.by", "�� �������� ���������. � ���� �� ������� ������� ������������� �������� ������!", "�� �������� ���������. ��� ��: " + kod);
                    email.sendMail();
                }
            } catch (IOException e) {
                LOG.error("������ ��� ������ ���� - " + e.getMessage());
            }
        }
    }
}
