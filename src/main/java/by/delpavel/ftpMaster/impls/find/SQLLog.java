package by.delpavel.ftpMaster.impls.find;


import by.delpavel.ftpMaster.interfaces.Find;
import by.delpavel.ftpMaster.service.EmailService;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.*;


public class SQLLog implements Find {

    private final Logger LOG = Logger.getLogger(SQLLog.class);
    private String kod;
    private Set<File> files = new HashSet<>();


    public SQLLog(String kod) {

        this.kod = kod;
    }

    @Override
    public void find() {

        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        SimpleDateFormat formatMinute = new SimpleDateFormat("mm");
        Calendar differentHour = Calendar.getInstance(TimeZone.getTimeZone("GMT+03:00"));

        int currentHour = Integer.parseInt(formatHour.format(new Date()));
        int currentMinute = Integer.parseInt(formatMinute.format(new Date()));

        LOG.debug("���(�) ��: " + kod);
        LOG.debug("������� �����: " + currentHour + ":" + currentMinute);
        if (currentHour == 6 && currentMinute >= 42) {


            File file = new File(Config.properties.getProperty("catalogBD" + kod.substring(2, kod.indexOf(" "))));
            file = new File(file.getParent());
            LOG.debug("����� ������ sql ����� � �������� ����� - " + file.getPath());


            for (File fileDirectory : file.listFiles()) {
                if (fileDirectory.isDirectory() && fileDirectory.getPath().toLowerCase().contains("sql")) {

                    LOG.debug("������� ����� SQL - " + fileDirectory.getPath());
                    findFiles(fileDirectory);
                }
            }

            LOG.debug("���������� �������� sql ��� ������: " + files.size());
            int countSQLLog = 0;
            boolean sizeSQLLog = true;
            for (File fileSQLLog : files) {

                differentHour.setTimeInMillis(new Date().getTime() - new Date(fileSQLLog.lastModified()).getTime());

                if (differentHour.getTimeInMillis() / 1000 / 60 / 60 / 24 < 14) {
                    LOG.debug("������ sql ��� - " + fileSQLLog.getName());
                    countSQLLog++;
                    LOG.debug("�������� ������� ����� sql ���� - " + fileSQLLog.getName() + " - " + fileSQLLog.length());
                    if (fileSQLLog.length() > 1000000000) {
                        sizeSQLLog = false;
                    }
                }
            }
            LOG.debug("��������� sql �����- " + countSQLLog + "-" + (Config.properties.size() - 2) / 5);
            if (countSQLLog < (Config.properties.size() - 2) / 5) {
                LOG.error("���������� SQLLog �� ������������ ���������� ���, �������� ��������� �� email");
                new EmailService("helpdesk@interfarmax.by", "pbobrov@interfarmax.by", "���������� SQL ����� �� ������������ ���������� ���!", "�������� � SQLLog. ���(�) ��: " + kod).sendMail();
            }
            if (!sizeSQLLog) {
                LOG.error("������ SQL ���� ������ ���������������, �������� ��������� �� email");
                new EmailService("helpdesk@interfarmax.by", "pbobrov@interfarmax.by", "������� ������ SQL ����", "�������� � SQLLog. ���(�) ��: " + kod).sendMail();
            }
        }
    }

    private void findFiles(File filePatch) {


        File[] filesAndDirectories = filePatch.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().toLowerCase().endsWith("ldf") || pathname.isDirectory();
            }
        });

        if (filesAndDirectories != null) {
            for (File nextFile : filesAndDirectories) {
                if (nextFile.isDirectory()) {
                    findFiles(nextFile);
                    continue;
                }
                files.add(nextFile);
            }
        }

    }
}
