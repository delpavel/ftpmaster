package by.delpavel.ftpMaster.impls.find;


import by.delpavel.ftpMaster.interfaces.Find;
import by.delpavel.ftpMaster.service.EmailService;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


public class Backup implements Find {

    private final Logger LOG = Logger.getLogger(Backup.class);
    private String kod;
    private Set<File> files = new HashSet<>();


    public Backup(String kod) {

        this.kod = kod;
    }

    @Override
    public void find() {

        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        SimpleDateFormat formatMinute = new SimpleDateFormat("mm");
        Calendar differentHour = Calendar.getInstance(TimeZone.getTimeZone("GMT+03:00"));

        int currentHour = Integer.parseInt(formatHour.format(new Date()));
        int currentMinute = Integer.parseInt(formatMinute.format(new Date()));

        LOG.debug("���(�) ��: " + kod);
        LOG.debug("������� �����: " + currentHour + ":" + currentMinute);
        if (currentHour == 6 && currentMinute >= 42) {


            File file = new File(Config.properties.getProperty("catalogBD" + kod.substring(2,kod.indexOf(" "))));
            file = new File(file.getParent());
            LOG.debug("����� ������ ������ � �������� ����� - " + file.getPath());


            for (File fileDirectory : file.listFiles()) {
                if (fileDirectory.isDirectory() && fileDirectory.getPath().toLowerCase().contains("backup")) {

                    LOG.debug("������� ����� ������� - " + fileDirectory.getPath());
                    findFiles(fileDirectory);
                }
            }


            int countBackup = 0;
            boolean sizeBackup = true;
            for (File fileBackup : files) {

                differentHour.setTimeInMillis(new Date().getTime() - new Date(fileBackup.lastModified()).getTime());

                if (differentHour.getTimeInMillis() / 1000 / 60 / 60 < 24) {
                    LOG.debug("������ ����� - " + fileBackup.getName());
                    countBackup++;
                    LOG.debug("�������� ������� ����� ������ - " + fileBackup.getName() + " - " + fileBackup.length());
                    if (fileBackup.length() < 400000000) {
                        sizeBackup = false;
                    }
                }
            }
            LOG.debug("��������� ������� - " + countBackup + "-" + (Config.properties.size() - 2) / 5);
            if (countBackup != (Config.properties.size() - 2) / 5) {
                LOG.error("�� �������� �����, �������� ��������� �� email");
                new EmailService("helpdesk@interfarmax.by", "pbobrov@interfarmax.by", "�� ��������(�) �����(�)", "�������� � �������(�). ���(�) ��: " + kod).sendMail();
            }
            if (!sizeBackup) {
                LOG.error("������ ������ ������ ���������������, �������� ��������� �� email");
                new EmailService("helpdesk@interfarmax.by", "pbobrov@interfarmax.by", "��������� ������ ������(��)", "�������� � �������(�). ���(�) ��: " + kod).sendMail();
            }
        }
    }

    private void findFiles(File filePatch) {


        File[] filesAndDirectories = filePatch.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith("zip") || pathname.isDirectory();
            }
        });

        if (filesAndDirectories != null) {
            for (File nextFile : filesAndDirectories) {
                if (nextFile.isDirectory()) {
                    findFiles(nextFile);
                    continue;
                }
                files.add(nextFile);
            }
        }

    }
}
