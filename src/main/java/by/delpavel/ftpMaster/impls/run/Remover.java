package by.delpavel.ftpMaster.impls.run;


import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Run;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Remover implements Run {

    private static final Logger LOG = Logger.getLogger(Remover.class);

    @Override
    public void run() {

        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        int currentHour = Integer.parseInt(formatHour.format(new Date()));
        File file = new File("C:\\1CV7s");


        if (currentHour >= 9 && currentHour <= 22 || currentHour == 4) {
            if (!file.exists() || currentHour == 4) {
                LOG.debug("������ ��������� - " + Config.properties.getProperty("remover"));
                try {
                    Process p = Runtime.getRuntime().exec("cmd /c " + Config.properties.getProperty("ftpFolder") + "\\" + Config.properties.getProperty("remover"));
                    File startbatFile = new File(Config.properties.getProperty("ftpFolder") + "\\" + Config.properties.getProperty("remover"));
                    if (startbatFile.exists()) {

                        p.waitFor();

                    } else {
                        LOG.error("��� ����� - " + Config.properties.getProperty("ftpFolder") + "\\" + Config.properties.getProperty("remover"));
                    }

                } catch (InterruptedException | IOException e) {
                    LOG.error("������ ���������� - " + Config.properties.getProperty("ftpFolder") + "\\" + Config.properties.getProperty("remover"));
                }
            }
        }

    }
}
