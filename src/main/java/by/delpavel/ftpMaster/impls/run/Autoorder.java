package by.delpavel.ftpMaster.impls.run;


import by.delpavel.ftpMaster.interfaces.Run;
import by.delpavel.ftpMaster.service.BatService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Autoorder implements Run {


    private String kod;


    public Autoorder(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void run() {
        String nameFile = "Autozakaz" + kod + ".bat";
        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        int currentHour = Integer.parseInt(formatHour.format(new Date()));

        if (currentHour >= 9 && currentHour <= 22) {

            new BatService(nameFile).run();
        }
    }

}
