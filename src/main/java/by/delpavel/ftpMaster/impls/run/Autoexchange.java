package by.delpavel.ftpMaster.impls.run;


import by.delpavel.ftpMaster.service.BatService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Run;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Autoexchange implements Run {

    private static final Logger LOG = Logger.getLogger(Autoexchange.class);
    private String kod;


    public Autoexchange(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }


    @Override
    public void run() {
        String nameFile;
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        SimpleDateFormat formatMinute = new SimpleDateFormat("mm");
        int currentDay = Integer.parseInt(formatDay.format(new Date()));
        int currentHour = Integer.parseInt(formatHour.format(new Date()));
        int currentMinute = Integer.parseInt(formatMinute.format(new Date()));

        LOG.debug("obmen = " + Config.properties.getProperty("obmen" + kod));
        if (currentDay == 1 && (currentHour == 0 && currentMinute <= 17 || currentHour == 5 && currentMinute <= 17)) {
            nameFile = "TA" + kod + ".bat";
        } else {
            nameFile = "monopolno" + kod + ".bat";
        }
        if (currentHour == 18 && currentMinute <= 17) {
            nameFile = "AutoexchangePC" + kod + ".bat";
        }
        if (Config.properties.getProperty("obmen" + kod).equals("1")) {
            nameFile = "Autoexchange" + kod + ".bat";
        }

        LOG.debug("Bat ���� ��� ������� - " + nameFile);
        if ((currentHour == 0 && currentMinute <= 17) || currentHour == 5 && currentMinute <= 17
                || currentHour == 18 && currentMinute <= 17
                || Config.properties.getProperty("obmen" + kod).equals("1")) {

            new BatService(nameFile).run();
        }
    }

}
