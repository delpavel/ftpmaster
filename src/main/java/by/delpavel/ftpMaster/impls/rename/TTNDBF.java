package by.delpavel.ftpMaster.impls.rename;


import by.delpavel.ftpMaster.interfaces.Rename;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileFilter;

public class TTNDBF implements Rename {


    private final Logger LOG = Logger.getLogger(TTNDBF.class);
    private String kod;


    public TTNDBF(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void rename() {

        if (Config.properties.getProperty("ttn" + kod).equals("1")) {

            File filesPath = new File(Config.properties.getProperty("catalogBD" + kod) + "\\TTN_IN\\");
            File[] filesList = filesPath.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().contains(kod);
                }
            });
            if (filesList != null) {
                LOG.debug("������� ������ ��� � ���������� - " + filesList.length);
                for (int i = 0; i < filesList.length; i++) {
                    String fileEtension = filesList[i].getName();
                    fileEtension = fileEtension.replace(Config.properties.getProperty("kod" + kod).substring(1), ".dbf");
                    LOG.debug("���������� ����� ��� �� - " + Config.properties.getProperty("kod" + kod).substring(1));
                    if (new File(filesPath + "\\" + fileEtension).exists()) {
                        LOG.debug("������� ������� ����������� ��� - " + fileEtension);
                        new File(filesPath + "\\" + fileEtension).delete();
                    }
                    LOG.debug("�������� ���� ��� - " + fileEtension);
                    if (!filesList[i].renameTo(new File(Config.properties.getProperty("catalogBD" + kod) + "\\TTN_IN\\" + fileEtension))) {
                        LOG.debug("�� ���� ������������� ���� - " + filesList[i]);
                    }
                }
            }
        }
    }

}

