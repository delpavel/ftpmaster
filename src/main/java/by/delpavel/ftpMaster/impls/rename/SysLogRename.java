package by.delpavel.ftpMaster.impls.rename;


import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Rename;
import org.apache.log4j.Logger;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class SysLogRename implements Rename {

    private final Logger LOG = Logger.getLogger(SysLogRename.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private String kod;

    public SysLogRename(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void rename() {

        SimpleDateFormat formatHour = new SimpleDateFormat("HH");
        formatHour.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
        int currentHour = Integer.parseInt(formatHour.format(new Date()));

        LOG.debug("������ ������� �� �������������� ����� - " + Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\1cv7.mlg");
        if (currentHour >= 22) {
            File fileMlg = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\1cv7.mlg");
            File fileLinks = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\links.tmp");
            if (fileMlg.length() > 225000000) {
                LOG.debug("���� ���� �������� - " + fileMlg.length());
                if (fileLinks.exists()) {
                    LOG.debug("������� �������� �����  - " + fileLinks.getPath());
                    fileLinks.delete();
                    LOG.debug("���� " + fileLinks.getName() + " ����� ������� ��������, ���������� - " + fileLinks.exists());
                }

                if (!fileLinks.exists()) {
                    LOG.debug("������� �������������� ����� ���� - " + fileMlg.getPath());
                    fileMlg.renameTo(new File(Config.properties.getProperty("catalogBD" + kod) + "\\Syslog\\" + sdf.format(new Date()) + ".mlg"));
                } else {
                    LOG.error("������ �������������� 1cv7.mlg");
                }
            }

        }
    }
}
