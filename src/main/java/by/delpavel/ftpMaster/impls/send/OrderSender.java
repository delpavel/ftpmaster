package by.delpavel.ftpMaster.impls.send;


import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Submit;
import org.apache.log4j.Logger;

import java.io.*;

public class OrderSender implements Submit {

    private final Logger LOG = Logger.getLogger(OrderSender.class);
    private String kod;


    public OrderSender(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void send() {

        File filesPath = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Orders\\");
        File[] filesList = filesPath.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.getName().endsWith(".ord") || pathname.getName().endsWith(".opt"));

            }
        });

        if (filesList.length > 0) {
            LOG.debug("�������� dat ������ �� ftp � ���������� - " + filesList.length);
            String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\Orders\\", "orders").sendFiles(filesList);
            if (result.contains("������")) {
                LOG.error(result);
            } else {
                for (int i = 0; i < filesList.length; i++) {
                    new File(filesList[i].getName()).delete();
                }
            }
        }

    }

}
