package by.delpavel.ftpMaster.impls.send;


import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Submit;
import org.apache.log4j.Logger;

import java.io.File;

public class PCSender implements Submit {

    private final Logger LOG = Logger.getLogger(PCSender.class);
    private String kod;


    public PCSender(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void send() {

        File file = new File(Config.properties.getProperty("catalogBD" + kod) + "\\PC\\" + Config.properties.getProperty("filePC" + kod));

        if (file.exists()) {
            LOG.debug("�������� ����� - " + Config.properties.getProperty("filePC" + kod));
            String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\PC\\", "for_office").sendFile(file);
            if (result.contains("������")) {
                LOG.error(result);
            } else {
                if (file.exists()) {
                    file.delete();
                }
            }
        }


    }
}
