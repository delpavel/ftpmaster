package by.delpavel.ftpMaster.impls.send;


import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Submit;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class TxtSender implements Submit {


    private final Logger LOG = Logger.getLogger(TxtSender.class);
    private String kod;

    public TxtSender() {
    }

    public TxtSender(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void send() {

        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
        formatTime.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));

        if (Config.properties.getProperty("kod" + kod) == null) {
            LOG.error("������ ��������� ���� �� ����� option.ini");
            return;
        }
        File file = new File(Config.properties.getProperty("ftpFolder") + "\\" + kod + ".txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                LOG.error("������ �������� ����� - " + file.getName());
                return;
            }
        }
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(formatDate.format(new Date()) + "\n");
            bufferedWriter.write(formatTime.format(new Date()) + "\n");
            bufferedWriter.write("280616" + "\n");
            bufferedWriter.close();
        } catch (IOException e) {
            LOG.error("�� ������ ���� - " + file.getName());
            return;
        }


        LOG.debug("�������� ����� - " + file);
        String result = new FtpService(Config.properties.getProperty("ftpFolder") + "\\", "ftpdemo").sendFile(file);
        if (result.contains("������")) {
            LOG.error(result);
        } else {
            if (file.exists()) {
                file.delete();
            } else {
                LOG.error("������ �������� ����� - " + file.getName());
            }
        }

    }
}
