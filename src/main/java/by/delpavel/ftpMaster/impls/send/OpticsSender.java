package by.delpavel.ftpMaster.impls.send;


import by.delpavel.ftpMaster.interfaces.Submit;
import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileFilter;

public class OpticsSender implements Submit {

    private final Logger LOG = Logger.getLogger(OpticsSender.class);
    private String kod;


    public OpticsSender(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void send() {

        File filesPath = new File(Config.properties.getProperty("catalogBD" + kod) + "\\optika_out\\");
        if (filesPath.exists()) {

            LOG.debug("������������ ������ ������ ������ ��� �������� �� ftp, �� �������� - " + filesPath.getPath());
            File[] filesList = filesPath.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return !(pathname.getName().endsWith(".tmp") || pathname.isDirectory());

                }
            });

            if (filesList.length > 0) {
                LOG.debug("�������� ������ ������ �� ftp � ���������� - " + filesList.length);
                String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\optika_out\\", "obmen_optika").sendFiles(filesList);
                if (result.contains("������")) {
                    LOG.error(result);
                } else {
                    for (int i = 0; i < filesList.length; i++) {
                        new File(filesList[i].getName()).delete();
                    }
                }
            } else {
                LOG.debug("C����� ������ ��� ������ ����!");
            }
        }

    }

}
