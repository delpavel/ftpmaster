package by.delpavel.ftpMaster.impls.send;


import by.delpavel.ftpMaster.service.FtpService;
import by.delpavel.ftpMaster.start.Config;
import by.delpavel.ftpMaster.interfaces.Submit;
import org.apache.log4j.Logger;

import java.io.*;


public class Check implements Submit {

    private final Logger LOG = Logger.getLogger(Check.class);
    private String kod;


    public Check(String kod) {

        this.kod = kod.substring(kod.indexOf(".") + 1);
    }

    @Override
    public void send() {

        File filesPath = new File(Config.properties.getProperty("catalogBD" + kod) + "\\Checks\\");
        File[] filesList = filesPath.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".dat");
            }
        });

        if (filesList.length > 0) {
            LOG.debug("�������� ����� �� ftp � ���������� - " + filesList.length);
            String result = new FtpService(Config.properties.getProperty("catalogBD" + kod) + "\\Checks\\", "cheki").sendFiles(filesList);
            if (result.contains("������")) {
                LOG.error(result);
            } else {
                for (int i = 0; i < filesList.length; i++) {
                    new File(filesList[i].getName()).delete();
                }
            }
        }
    }
}
