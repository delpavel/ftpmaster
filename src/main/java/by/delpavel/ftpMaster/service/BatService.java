package by.delpavel.ftpMaster.service;


import by.delpavel.ftpMaster.interfaces.Run;
import by.delpavel.ftpMaster.start.Config;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class BatService implements Run {

    private static final Logger LOG = Logger.getLogger(BatService.class);
    private String nameFile;


    public BatService(String nameFile) {

        this.nameFile = nameFile;
    }


    @Override
    public void run() {

        LOG.debug("������ bat ����� - " + nameFile);
        try {
            Process p = Runtime.getRuntime().exec("cmd /c " + Config.properties.getProperty("ftpFolder") + "\\" + nameFile);
            File startbatFile = new File(Config.properties.getProperty("ftpFolder") + "\\" + nameFile);
            if (startbatFile.exists()) {

                p.waitFor();

            } else {
                LOG.error("��� bat ����� - " + Config.properties.getProperty("ftpFolder") + "\\" + nameFile);
                throw new FileNotFoundException();
            }


        } catch (InterruptedException | IOException e) {
            LOG.error("������ - " + e.getMessage());
        }
    }


}
