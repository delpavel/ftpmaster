package by.delpavel.ftpMaster.service;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailService {

    private String to;
    private String from;
    private String message;
    private String subject;
    private final Logger LOG = Logger.getLogger(EmailService.class);

    public EmailService(String to, String from, String message, String subject) {
        this.to = to;
        this.from = from;
        this.message = message;
        this.subject = subject;

    }

    public void sendMail() {

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", "10.12.1.7");
        properties.put("mail.smtp.port", "25");


        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("pbobrov", "456");
            }
        });

        try {
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(from));
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mimeMessage.setSubject(subject);
            mimeMessage.setText(message);

            Transport.send(mimeMessage);
            LOG.debug("Email ���������");
        } catch (MessagingException e) {
            LOG.error("������ �������� email - " + e.getMessage());

        }
    }
}
