package by.delpavel.ftpMaster.service;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import java.io.*;


public class FtpService {

    private String patchFile;
    private String workingDirectory;
    private final Logger LOG = Logger.getLogger(FtpService.class);

    public FtpService(String patchFile, String workingDirectory) {
        this.patchFile = patchFile;
        this.workingDirectory = workingDirectory;
    }

    public String sendFile(File file) {
        String server = "10.12.1.8";
        int port = 21;
        String user = "helpdesk";
        String pass = "helpdesk";
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                return " ������ �������: " + replyCode;
            }
            boolean success = ftpClient.login(user, pass);
            if (!success) {
                return "������. �� ���� ������������ � FTP �������!";
            } else {
                ftpClient.enterLocalPassiveMode();
                InputStream in;
                ftpClient.changeWorkingDirectory(workingDirectory);
                if (file.exists()) {
                    in = new FileInputStream(file);
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    if (!ftpClient.storeFile(file.getName(), in)) {
                        LOG.error(" ������ �������� ����� - " + file.getName());
                        ftpClient.logout();
                        ftpClient.disconnect();
                        in.close();
                        return " ������ �������� ����� - " + file.getName();
                    } else {
                        LOG.info("������� ���� - " + file.getName());
                    }
                } else {
                    LOG.error("���� �� ������ - " + file.getName());
                    ftpClient.logout();
                    ftpClient.disconnect();
                    return "������. ���� �� ������ - " + file.getName();
                }

                ftpClient.logout();
                ftpClient.disconnect();
                in.close();

            }
        } catch (IOException ex) {
            return " ������ ��� �������!";
        }
        return "��������� ��� ������";
    }

    public String sendFiles(File[] filesList) {
        String server = "10.12.1.8";
        int port = 21;
        String user = "helpdesk";
        String pass = "helpdesk";
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                return " ������ �������: " + replyCode;
            }
            boolean success = ftpClient.login(user, pass);
            if (!success) {
                return "������. �� ���� ������������ � FTP �������!";
            } else {
                ftpClient.enterLocalPassiveMode();
                ftpClient.changeWorkingDirectory(workingDirectory);
                for (int i = 0; i < filesList.length; i++) {
                    if (filesList[i].exists()) {
                        ftpClient.deleteFile(filesList[i].getName());
                        InputStream in = new FileInputStream(filesList[i]);
                        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                        if (!ftpClient.storeFile(filesList[i].getName(), in)) {
                            LOG.error(" ������ �������� ����� - " + filesList[i].getName());
                        } else {
                            LOG.info("������� ���� - " + filesList[i].getName());
                        }
                        in.close();
                        if (filesList[i].exists()) {
                            if (!filesList[i].delete()) {
                                LOG.error("������ �������� ����� - " + filesList[i].getName());
                            }
                        }
                    } else {
                        LOG.error("���� �� ������ - " + filesList[i].getName());
                    }
                }
                ftpClient.logout();
                ftpClient.disconnect();

            }
        } catch (IOException ex) {
            return " ������ ��� �������!";
        }
        return "��������� ��� ������";
    }

    public String getFile(String outFile) {
        String server = "10.12.1.8";
        int port = 21;
        String user = "helpdesk";
        String pass = "helpdesk";
        FTPClient ftpClient = new FTPClient();
        FTPFile file;
        try {
            ftpClient.connect(server, port);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                return " ������ �������: " + replyCode;
            }
            boolean success = ftpClient.login(user, pass);
            if (!success) {
                return "������. �� ���� ������������ � FTP �������!";
            } else {
                ftpClient.enterLocalPassiveMode();
                ftpClient.changeWorkingDirectory(workingDirectory);
                file = ftpClient.mdtmFile(outFile);
                if (file != null && file.getName().equals(outFile)) {
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    OutputStream outputStream = new FileOutputStream(patchFile + file.getName());
                    if (!ftpClient.retrieveFile(file.getName(), outputStream)) {
                        {
                            ftpClient.logout();
                            ftpClient.disconnect();
                            return " ������ �������� ����� - " + file.getName();
                        }

                    } else {
                        LOG.info("������� ���� - " + file.getName());
                    }
                    ftpClient.deleteFile(file.getName());
                    outputStream.close();
                }

                ftpClient.logout();
                ftpClient.disconnect();

            }
        } catch (IOException e) {
            return " ������ ��� ������� - " + e.getMessage();
        }
        return "��������� ��� ������ - " + file;
    }

    public String getFiles(String kod) {
        String server = "10.12.1.8";
        int port = 21;
        String user = "helpdesk";
        String pass = "helpdesk";
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                return " ������ �������: " + replyCode;
            }
            boolean success = ftpClient.login(user, pass);
            if (!success) {
                return "������. �� ���� ������������ � FTP �������!";
            } else {
                ftpClient.enterLocalPassiveMode();
                ftpClient.changeWorkingDirectory(workingDirectory);
                FTPFile[] files = ftpClient.listFiles(kod);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                for (FTPFile file : files) {
                    if (file.getName().endsWith(kod.substring(1))) {
                        OutputStream outputStream = new FileOutputStream(patchFile + file.getName());
                        if (!ftpClient.retrieveFile(file.getName(), outputStream)) {
                            {
                                ftpClient.logout();
                                ftpClient.disconnect();
                                return " ������ �������� ����� - " + file.getName();
                            }

                        } else {
                            LOG.info("������� ���� - " + file.getName());
                        }
                        ftpClient.deleteFile(file.getName());
                        outputStream.close();
                    }
                }


                ftpClient.logout();
                ftpClient.disconnect();


            }
        } catch (IOException e) {
            return " ������ ��� ������� - " + e.getMessage();
        }
        return "��������� ��� ������";
    }

}
